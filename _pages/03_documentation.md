---
layout: page
title: Documentation
permalink: /documentation/
order: 4
redirect_from:
  # We want to redirect from /Documentation.php. We need ".php.html" else the
  # redirect page is downloaded to ~/Downloads/ rather than shown in browser. See:
  # https://github.com/jekyll/jekyll-redirect-from/issues/145#issuecomment-392277818
  - /Documentation.php.html
---

<dl>
<dt><b>On-line reference pages</b></dt>
<dd><a href="/doc/info/lang.html">The DOT Language</a></dd>
<dd><a href="/doc/info/command.html">Command-line Usage</a></dd>
<dd><a href="/doc/info/output.html">Output Formats</a></dd>
<dd><a href="/doc/info/attrs.html">Node, Edge and Graph Attributes</a></dd>
<dd><a href="/doc/info/shapes.html">Node Shapes</a></dd>
<dd><a href="/doc/info/arrows.html">Arrow Shapes</a></dd>
<dd><a href="/doc/info/colors.html">Colors</a></dd>

<dt><b>Schema Files</b> (<a href="http://www.w3.org/XML/Schema">XSD</a> format)</dt>
<dd><a href="/doc/info/graphviz_json_schema.json">Schema</a> for <a href="/doc/info/output.html#d:json">json output</a></dd>

<dt><b>User's Guides</b> 
<dd><b>NOTE:</b> The first two documents are not current with the features
and details of Graphviz. They can serve as tutorials for understanding how
to use Graphviz, but the most up-to-date documentation is provided in the
on-line pages listed above.</dd>
<dd><a href="/pdf/dotguide.pdf">dot</a></dd>
<dd><a href="/pdf/neatoguide.pdf">neato</a></dd>
<dd><a href="/pdf/leftyguide.pdf">lefty</a></dd>
<dd><a href="/pdf/dottyguide.pdf">dotty</a></dd>
<dd><a href="/pdf/cgraph.pdf">Cgraph library tutorial</a></dd>
<dd><a href="/pdf/libguide.pdf">Using Graphviz as a library</a></dd>
<dd><a href="/pdf/oldlibguide.pdf">Using Graphviz as a library (pre-2.30 version)</a></dd>
<dd><a href="/doc/addingLayout.txt">Adding a new layout</a></dd>

<dt><b>Sample Programs using Graphviz</b> 
<dd><a href="/dot.demo/demo.c">demo.c</a></dd>
<dd><a href="/dot.demo/dot.c">dot.c</a></dd>
<dd><a href="/dot.demo/example.c">example.c</a></dd>
<dd><a href="/dot.demo/simple.c">simple.c</a></dd>
<dd><a href="/dot.demo/Makefile">Makefile</a></dd>

<dt><b>Layout manual pages</b> 
<dd><a target="_blank" href="/pdf/dot.1.pdf">circo</a>
<dd><a target="_blank" href="/pdf/dot.1.pdf">dot</a>
<dd><a target="_blank" href="/pdf/dot.1.pdf">fdp</a>
<dd><a target="_blank" href="/pdf/dot.1.pdf">neato</a>
<dd><a target="_blank" href="/pdf/osage.1.pdf">osage</a>
<dd><a target="_blank" href="/pdf/patchwork.1.pdf">patchwork</a>
<dd><a target="_blank" href="/pdf/dot.1.pdf">sfdp</a>
<dd><a target="_blank" href="/pdf/dot.1.pdf">twopi</a>

<dt><b>Tool manual pages</b> 
<dd> <a target="_blank" href="/pdf/acyclic.1.pdf">acyclic</a>
<a target="_blank" href="/pdf/bcomps.1.pdf">bcomps</a>
<a target="_blank" href="/pdf/cluster.1.pdf">cluster</a>
<a target="_blank" href="/pdf/ccomps.1.pdf">ccomps</a>
<a target="_blank" href="/pdf/diffimg.1.pdf">diffimg</a>
<dd>
<a target="_blank" href="/pdf/dijkstra.1.pdf">dijkstra</a>
<a target="_blank" href="/pdf/dotty.1.pdf">dotty</a>
<a target="_blank" href="/pdf/edgepaint.1.pdf">edgepaint</a>
<a target="_blank" href="/pdf/gc.1.pdf">gc</a>
<a target="_blank" href="/pdf/gml2gv.1.pdf">gml2gv</a> 
<dd>
<a target="_blank" href="/pdf/graphml2gv.1.pdf">graphml2gv</a> 
<a target="_blank" href="/pdf/gxl2gv.1.pdf">gv2gxl</a>
<a target="_blank" href="/pdf/gvcolor.1.pdf">gvcolor</a>
<a target="_blank" href="/pdf/gvedit.1.pdf">gvedit</a>
<a target="_blank" href="/pdf/gvgen.1.pdf">gvgen</a>
<dd>
<a target="_blank" href="/pdf/gvmap.1.pdf">gvmap</a>
<a target="_blank" href="/pdf/gvpack.1.pdf">gvpack</a>
<a target="_blank" href="/pdf/gvpr.1.pdf">gvpr</a>
<a target="_blank" href="/pdf/gxl2gv.1.pdf">gxl2gv</a> 
<a target="_blank" href="/pdf/lefty.1.pdf">lefty</a>
<dd>
<a target="_blank" href="/pdf/lneato.1.pdf">lneato</a> 
<a target="_blank" href="/pdf/mingle.1.pdf">mingle</a>
<a target="_blank" href="/pdf/mm2gv.1.pdf">mm2gv</a>
<a target="_blank" href="/pdf/nop.1.pdf">nop</a>
<a target="_blank" href="/pdf/sccmap.1.pdf">sccmap</a>
<dd>
<a target="_blank" href="/pdf/smyrna.1.pdf">smyrna</a>
<a target="_blank" href="/pdf/tred.1.pdf">tred</a>
<a target="_blank" href="/pdf/unflatten.1.pdf">unflatten</a>
<a target="_blank" href="/pdf/vimdot.1.pdf">vimdot</a>

<dt><b>C libraries</b> 
<dd><a href="/pdf/cdt.3.pdf" target="_blank">cdt</a>
<dd><a href="/pdf/cgraph.3.pdf" target="_blank">cgraph</a>
<dd><a href="/pdf/gvc.3.pdf" target="_blank">gvc</a>
<dd><a href="/pdf/pack.3.pdf" target="_blank">pack</a>
<dd><a href="/pdf/pathplan.3.pdf" target="_blank">pathplan</a>
<dd><a href="/pdf/xdot.3.pdf" target="_blank">xdot</a>

<dt><b>Tcl/tk libraries</b> 
<dd><a target="_blank" href="/pdf/gdtclft.3tcl.pdf">gdtclft.3tcl</a>
<dd><a target="_blank" href="/pdf/tcldot.3tcl.pdf">tcldot.3tcl</a>
<dd><a target="_blank" href="/pdf/tkspline.3tk.pdf">tkspline.3tk</a>

<dt><b>Scripting APIs</b> 
<dd><a target="_blank" href="/pdf/gv.3guile.pdf">gv.3guile</a>
<dd><a target="_blank" href="/pdf/gv.3java.pdf">gv.3java</a>
<dd><a target="_blank" href="/pdf/gv.3perl.pdf">gv.3perl</a>
<dd><a target="_blank" href="/pdf/gv.3php.pdf">gv.3php</a>
<dd><a target="_blank" href="/pdf/gv.3python.pdf">gv.3python</a>
<dd><a target="_blank" href="/pdf/gv.3ruby.pdf">gv.3ruby</a>
<dd><a target="_blank" href="/pdf/gv.3tcl.pdf">gv.3tcl</a>

<dt><b>Open problems</b> 
<dd><a target="_blank" href="/doc/todo.html">Graphviz Wish List</a>
